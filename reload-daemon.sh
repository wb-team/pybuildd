#!/bin/bash

set -e

export XDG_RUNTIME_DIR="/run/user/$(id -u)"
export DBUS_SESSION_BUS_ADDRESS="/run/user/$(id -u)/bus"

systemctl --user daemon-reload
systemctl --user kill --kill-whom=main --signal=SIGUSR1 buildd.service
